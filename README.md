# WikiLanguages

## Python Notebook to expand external MediaWikis with languages links into corresponding Wikipedia articles

This Python Notebook expands external MediaWikis with links to corresponding foreign language Wikipedia versions, based on the Wikidata-Item containing an external propoerty for the target MediaWiki.